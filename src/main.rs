#![no_main]
#![no_std]
#![feature(negative_impls)]

use log::*;
use uefi::data_types::{CStr8, Char8};
use uefi::prelude::*;
use uefi::proto::unsafe_protocol;
use uefi::table::boot::{OpenProtocolAttributes, OpenProtocolParams};
use uefi::Result;

#[derive(Copy, Clone)]
#[repr(packed)]
pub struct SmbiosTableHeader {
    pub ty: u8,
    pub len: u8,
    pub handle: u16,
}

#[derive(Copy, Clone)]
#[repr(packed)]
pub struct OnboardDevicesExtendedInformation {
    pub header: SmbiosTableHeader,
    pub reference_designation: u8,
    pub device_type: u8,
    pub device_type_instance: u8,
    pub segment_group_number: u16,
    pub bus_number: u8,
    pub device_function_number: u8,
}

#[repr(C)]
pub struct WithStrings<I, const N: usize> {
    pub inner: I,
    pub trailer: [u8; N],
}

#[repr(C)]
#[unsafe_protocol("03583ff6-cb36-4940-947e-b9b39f4afaf7")]
pub struct Smbios {
    add: extern "efiapi" fn(
        this: *const Smbios,
        producer_handle: Option<Handle>,
        smbios_handle: *mut u16,
        record: *const SmbiosTableHeader,
    ) -> Status,
    update_string: extern "efiapi" fn(
        this: *const Smbios,
        smbios_handle: *mut u16,
        string_number: *const usize,
        string: *const Char8,
    ) -> Status,
    remove: extern "efiapi" fn(this: *const Smbios, smbios_handle: u16) -> Status,
    get_next: extern "efiapi" fn(
        this: *const Smbios,
        smbios_handle: *mut u16,
        ty: *const u8,
        record: *mut *mut SmbiosTableHeader,
        producer_handle: *mut Option<Handle>,
    ) -> Status,
    major_version: u8,
    minor_version: u8,
}

#[allow(clippy::missing_safety_doc)]
impl Smbios {
    pub unsafe fn add(
        &self,
        producer_handle: Option<Handle>,
        smbios_handle: *mut u16,
        record: *const SmbiosTableHeader,
    ) -> Result {
        (self.add)(self, producer_handle, smbios_handle, record).to_result()
    }

    pub unsafe fn update_string(
        &self,
        smbios_handle: *mut u16,
        string_number: usize,
        string: &CStr8,
    ) -> Result {
        (self.update_string)(self, smbios_handle, &string_number, string.as_ptr()).to_result()
    }

    pub unsafe fn remove(&self, smbios_handle: u16) -> Result {
        (self.remove)(self, smbios_handle).to_result()
    }

    pub unsafe fn get_next(
        &self,
        smbios_handle: *mut u16,
        ty: Option<u8>,
        record: *mut *mut SmbiosTableHeader,
        producer_handle: *mut Option<Handle>,
    ) -> Result {
        (self.get_next)(
            self,
            smbios_handle,
            ty.as_ref()
                .map(|x| x as *const u8)
                .unwrap_or(core::ptr::null()),
            record,
            producer_handle,
        )
        .to_result()
    }

    pub fn major_version(&self) -> u8 {
        self.major_version
    }

    pub fn minor_version(&self) -> u8 {
        self.minor_version
    }
}

#[entry]
fn main(image: Handle, mut system_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut system_table).unwrap();

    let bt = system_table.boot_services();

    let handle = bt.get_handle_for_protocol::<Smbios>().unwrap();

    let smbios = unsafe {
        &mut bt
            .open_protocol::<Smbios>(
                OpenProtocolParams {
                    handle,
                    agent: image,
                    controller: None,
                },
                OpenProtocolAttributes::GetProtocol,
            )
            .unwrap()
    };

    let enp4s0 = OnboardDevicesExtendedInformation {
        header: SmbiosTableHeader {
            ty: 41,
            len: 0x0B,
            handle: 0xFFFE,
        },
        reference_designation: 1,
        device_type: 0x85,
        device_type_instance: 0,
        segment_group_number: 0x0000,
        bus_number: 0x04,
        device_function_number: 0x00,
    };

    let wlp5s0 = OnboardDevicesExtendedInformation {
        device_type: 0x8B,
        bus_number: 0x05,
        ..enp4s0
    };

    let enp6s0 = OnboardDevicesExtendedInformation {
        device_type_instance: 1,
        bus_number: 0x06,
        ..enp4s0
    };

    let enp4s0_wrapper = WithStrings {
        inner: enp4s0,
        trailer: *b"enp4s0\0\0",
    };

    let wlp5s0_wrapper = WithStrings {
        inner: wlp5s0,
        trailer: *b"wlp5s0\0\0",
    };

    let enp6s0_wrapper = WithStrings {
        inner: enp6s0,
        trailer: *b"enp6s0\0\0",
    };

    unsafe {
        for (name, wrapper) in [
            ("enp4s0", enp4s0_wrapper),
            ("wlp5s0", wlp5s0_wrapper),
            ("enp6s0", enp6s0_wrapper),
        ] {
            debug!("Adding {}", name);
            let mut smbios_handle = 0xFFFE;
            smbios
                .add(None, &mut smbios_handle, &wrapper.inner.header)
                .unwrap();
        }
    }

    debug!("Appended SMBIOS tables");

    Status::SUCCESS
}
